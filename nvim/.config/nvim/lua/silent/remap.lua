-- Leader key
vim.g.mapleader = " "

-- Explorer
vim.keymap.set("n", "<leader>pv", vim.cmd.Ex)

-- Clipboard

