alias ls='ls --color=auto'
alias ll='ls -l'
alias la='ls -lA'
alias l='ls -CF'

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

alias rm='rm -I'
alias mv='mv -i'
alias cp='cp -i'
alias chmod='chmod --preserve-root'

